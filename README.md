# readmfg tool implementation

This tool can be used to read the Secured Manufacturing Data (MFG_DATA).

## Functionality:
Return the value of a MFG field.

## Usage:
Usage: readmfg [-sShHmMrRbBpPfli] [FIELD]

Dump MFG parameters

Parameters
        -s -S           Returns a string
        -h -H           Returns a hex number string
        -m -M [:-]      Returns a hex array in format xx:xx:xx:xx:xx:xx or xx-xx-xx-xx-xx-xx
        -r -R           Returns raw binary data
        -p -P           Returns PON_SERIAL in a human readable format.
        -f    [FILE]    Dump data to a file instead of stdout
        -l              List all valid name strings.
        -i              Show this menu
        -v              Verbose

## Features that could be implemented:
- [] Add support for Read Write MFG
- [] Reading from mtd device
- [] Support hex2bin (if necessary)
- [] Add Support for blacklisted items