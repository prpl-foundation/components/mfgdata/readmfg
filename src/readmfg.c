/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <mfg_api.h>

#define DEBUG(fmt, args ...) if(debug) { fprintf(stderr, "%s:%d: DEBUG - " fmt, \
                                                 __FILE__, __LINE__, ## args); }

#define ERROR(fmt, args ...) do { fprintf(stderr, "%s:%d: ERROR - " fmt,  \
                                          __FILE__, __LINE__, ## args); } while(0)

typedef enum format_e {
    UNKNOWN,
    STRING,
    HEX,
    MAC,
    RAW,
    PON,
} format_t;

static int debug = 0;

static void usage(FILE* stream, const char* prog_name) {
    fprintf(stream, "Usage: %s [-sShHmMrRbBpPfli] [FIELD]\n", prog_name);
    fprintf(stream, "\n\nDump MFG parameters\n\nParameters\n");
    fprintf(stream, "\t-s -S\t\tReturns a string\n");
    fprintf(stream, "\t-h -H\t\tReturns a hex number string\n");
    fprintf(stream, "\t-m -M [:-]\tReturns a hex array in format xx:xx:xx:xx:xx:xx or xx-xx-xx-xx-xx-xx\n");
    fprintf(stream, "\t-r -R\t\tReturns raw binary data\n");
    fprintf(stream, "\t-p -P\t\tReturns PON_SERIAL in a human readable format.\n");
    fprintf(stream, "\t-f    [FILE]\tDump data to a file instead of stdout\n");
    fprintf(stream, "\t-l\t\tList all valid name strings.\n");
    fprintf(stream, "\t-i\t\tShow this menu\n");
    fprintf(stream, "\t-v\t\tVerbose\n");
}



int main (int argc, char* argv[]) {
    char* requested_field = "";
    int ret, c, count = 0;
    unsigned char separator = ':';
    unsigned char* buffer = NULL;
    unsigned long length = 0;
    format_t format = UNKNOWN;
    FILE* file = NULL;

    // This is the pointer to the head node of the linked list
    struct mfg_entry* head = NULL;

    requested_field = argv[argc - 1];

    while(-1 != (c = getopt(argc, argv, "sShHm::M::rRpPf:liv"))) {
        switch(c) {
        case 's':
        case 'S':
            format = STRING;
            break;

        case 'h':
        case 'H':
            format = HEX;
            break;

        case 'm':
        case 'M':
            format = MAC;
            if(optarg && (strlen(optarg) == 1)) {
                separator = optarg[0];
                DEBUG("Separator set to %c\n", separator);
            }
            break;

        case 'r':
        case 'R':
            format = RAW;
            break;

        // This case is only for PON_SERIAL.
        // It will display the first 4 bytes as a string and the last 4 bytes as a hex value.
        case 'p':
        case 'P':
            format = PON;
            requested_field = "PON_SERIAL";
            break;

        case 'f':
            if(NULL == (file = fopen(optarg, "w+b"))) {
                ERROR("Failed opening %s: %s\n", optarg, strerror(errno));
                ret = EXIT_FAILURE;
                goto end;
            }
            break;

        case 'l':
        {
            // First the linked list needs to be created!
            if((ret = mfg_init(&head))) {
                ERROR("INTERNAL ERROR: mfg_init returned error code %d!\n", ret);
                ret = EXIT_FAILURE;
                goto end;
            }

            // Get all the names
            char** names = mfg_getMfgEntryNames(head, &count);

            // Print out all the names of the nodes in the linked listy
            fprintf(stdout, "All available MFG fields:\n");
            for(int i = 0; i < count; i++) {
                fprintf(stdout, "\t%s\n", names[i]);
            }

            // Free the allocated memory for the names list
            free(names);

            ret = EXIT_SUCCESS;
            goto end;
        }

        case 'i':
            usage(stdout, argv[0]);
            ret = EXIT_SUCCESS;
            goto end;

        case 'v':
            debug = 1;
            break;

        default:
            if(optind != (argc - 1)) {
                usage(stderr, argv[0]);
                ret = EXIT_FAILURE;
                goto end;
            }
        }
    }

    if((optind != (argc - 1)) && (format != PON)) {
        ERROR("Missing field\n");
        usage(stderr, argv[0]);
        ret = EXIT_FAILURE;
        goto end;
    }

    // Check if the given field is not NULL
    if(requested_field == NULL) {
        ERROR("No correct input field is provided");
        ret = EXIT_FAILURE;
        goto end;
    }

    if((ret = mfg_init(&head))) {
        ERROR("INTERNAL ERROR: mfg_init returned error code %d!\n", ret);
        ret = EXIT_FAILURE;
        goto end;
    }

    DEBUG("mfg_init() was successfull\n");
    DEBUG("Retrieving the MFG field %s !\n", requested_field);

    if((ret = mfg_read(requested_field, &head, (void**) &buffer, &length))) {
        ERROR("INTERNAL ERROR: mfg_read returned error code %d!\n", ret);
        ret = EXIT_FAILURE;
        goto end;
    }

    DEBUG("Got length %lu!\n", length);

    switch(format) {
    case STRING:
        fprintf(file ? file : stdout, "%.*s", (int) length, buffer);
        break;

    case HEX:
    {
        unsigned long i;
        for(i = 0; i < length; ++i) {
            fprintf(file ? file : stdout, "%02X", buffer[i]);
        }
        break;
    }

    case MAC:
    {
        unsigned long i;
        for(i = 0; i < (length - 1); ++i) {
            fprintf(file ? file : stdout, "%02X%c", buffer[i], separator);
        }
        fprintf(file ? file : stdout, "%02X", buffer[i]);
        break;
    }

    case PON:
    {
        unsigned long i;
        // The PON_SERIAL number should always have a length of 8
        if((length != 8) && strncmp(requested_field, "PON_SERIAL", strlen("PON_SERIAL"))) {
            ERROR("This value is not a valid PON_SERIAL number");
        } else {
            // Print the 4 first ASCII characters
            fprintf(file ? file : stdout, "%.*s", 4, buffer);

            // The rest of the value should be printed as hex
            for(i = 4; i < length; ++i) {
                fprintf(file ? file : stdout, "%02X", buffer[i]);
            }
        }

        break;
    }

    case RAW:
        fwrite(buffer, 1, length, file ? file : stdout);
        break;

    default:
        ERROR("Specify a format to output the data [sShHmMrRbB]\n");
        usage(stderr, argv[0]);
        ret = EXIT_FAILURE;
        goto end;
    }

end:
    mfg_free(&head);
    free(buffer);
    if(file) {
        fclose(file);
    }
    exit(ret);
}
